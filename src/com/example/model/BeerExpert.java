package com.example.model;

import java.util.LinkedList;
import java.util.List;


public class BeerExpert {

	
	public List<String> getBrands(String color) {
		List<String> brands = new LinkedList<String>();
		
		if (color.equals("amber")) {
			brands.add("Jack Amber");
			brands.add("Red Moose");
			brands.add("Bock");
		}
		else {
			brands.add("Jail Pale Ale");
			brands.add("Gout Stout");
			brands.add("Pilsen");
			brands.add("Weissen");
			brands.add("Dunkel");
		}
		
		
		
		return brands;
	}

}
